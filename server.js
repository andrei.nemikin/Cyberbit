var express = require('express');
var app = express();

var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'dbuser',
    password : 'password'
});

connection.connect();

app.get('/getUsers', function (req, res) {
    connection.query('SELECT * FROM Users', function(err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }
        res.send(rows);
    });

});

app.put('/updateUser', function (req, res) {
    let id = req.query.id;
    connection.query('UPDATE users SET(data) WHERE id='+id, function(err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }
        res.send(rows);
    });

});

app.post('/addUser', function (req, res) {
    connection.query('INSERT INTO (pic, name, active) VALUES (req.query.pic, req.query.name, req.query.active);',
        function(err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }
        res.send(rows);
    });

});

app.delete('/deleteUser', function (req, res) {
    let id = req.query.id;
    connection.query('DELETE FROM users WHERE id='+id, function(err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }
        res.send(rows);
    });
});

app.get('/getRoles', function (req, res) {
    connection.query('SELECT * FROM Roles', function(err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }
        res.send(rows);
    });
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});