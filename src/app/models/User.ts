
export class User {
  UserId: number;
  IsActive: boolean;
  Name: string;
  Icon: string;
  Roles: [{
            ID: number;
            Name: string;
          }]
}
