import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserListComponent} from "./user-list/user-list.component";
import {EditUserComponent} from "./edit-user/edit-user.component";


const routs: Routes = [
  {path: '', redirectTo: 'user-list', pathMatch: 'full'},
  {path: 'user-list', component: UserListComponent},
  {path: 'edit-user', component: EditUserComponent},
  {path: '**', redirectTo: 'UserListComponent', pathMatch: 'full'}
];

@NgModule({
  imports:[RouterModule.forRoot(routs)],
  exports: [RouterModule]
})


export class AppRoutingModule {}
