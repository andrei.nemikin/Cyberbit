import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UsersDataService {

  constructor(private http: HttpClient) {
  }

  getUsers() {
    return this.http.get('./assets/data.json')
  }

  updateUser(index) {

  }

  addUser(user) {

  }

  deleteUser(index) {

  }

  getRoles() {

  }
}
