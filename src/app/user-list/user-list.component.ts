import { Component, OnInit } from '@angular/core';
import {UsersDataService} from "../services/usersDataService";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: any[];
  constructor(private usersDataService: UsersDataService) { }

  ngOnInit() {
    this.usersDataService.getUsers().subscribe((users: any[]) => {
      this.users = users;
      console.log(users)
    });
  }

}
